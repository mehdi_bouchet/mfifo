INC_DIR = inc
SRC_DIR = src
OBJ_DIR = obj

CFLAGS  = -g -Wall -I./$(INC_DIR) 
LDFLAGS = 
GC		= gcc

SRCS	= main.c $(SRC_DIR)/mfile.c
OBJS	= $(OBJ_DIR)/main.o $(OBJ_DIR)/mfile.o
DEPS 	= $(INC_DIR)/mfile.h

EXEC	= main

all: $(EXEC)

$(EXEC): $(OBJS)
	$(GC) -o $(EXEC) $^ $(LDFLAGS) 

$(OBJ_DIR)/main.o:	main.c $(DEPS)
	$(GC) -c $(CFLAGS) -o $(OBJ_DIR)/main.o main.c

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(DEPS)
	$(GC) -c $(CFLAGS) -o $@ $<

clean:
	rm -rf $(OBJ_DIR)/*.o
	rm -rf $(EXEC)