#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "./inc/mfile.h"

static mfifo** fifos= NULL;
static int fifos_connected= 0;

static int mfifo_unlock_all(){
    int n= 0;
    for(int i=0; i<fifos_connected; i++){
        n=( ( n == 0 && mfifo_unlock(fifos[i]) == 0 && mfifo_unlock_write(fifos[i]) == 0) ? 0 : -1);
        printf("Unlock effectué sur la mFifo %s\n", fifos[i]->name);
    }
    return n;
}

static void deleteMfifo(mfifo *fifo){
    int id= -1;
    if( fifos_connected == 0 || fifos_connected == 1){
        fifos_connected= 0;
        return;
    }
    for(int i=0; i<fifos_connected; i++){
        if( fifos[i]->id == fifo->id){
            id= i;
            break;
        }
    }
    if(id >= fifos_connected){
        printf("Impossible de supprimer.");
        return; // impossible
    }
    for(int i=id; i<fifos_connected-1; i++)
        memmove(&fifos[i], &fifos[i+1], sizeof(mfifo*));
    fifos_connected--;
}

static int fifo_sendMessage(mfifo *fifo, void *data, size_t len){
    size_t long_tot = sizeof(message) + len;
    message *m = malloc(long_tot);

    m->l = len;
    memmove(m->mes, data, m->l) ;
    printf("[INFO] Message prêt à l'écriture sur la mFifo\n");
    printf("[INFO] %zu octets écrits. Message de %zu caractère(s)\n==> %s\n", long_tot, m->l, m->mes);
    return mfifo_write(fifo, m, long_tot);
}

static size_t fifo_receiveMessage(mfifo *fifo, message **ps){
    
    int occupied_space= fifo->capacity - mfifo_free_memory(fifo) - 1;
    if(occupied_space == 0){
        printf("Aucune donnée dans la mFifo.\n");
        return 0;
    }
    if( occupied_space < sizeof(message) ){
        printf("Aucun message dans la mFifo. Veuillez patienter.\n");
        return 0;
    }
    mfifo_lock(fifo);
    size_t n1, n2;
    
    if(*ps == NULL) (*ps)= malloc( sizeof(message) );
    
    printf("******* Lecture de la Taille *******\n");
    n1= mfifo_read(fifo, *ps, sizeof(message)); /* lire sans tableau mes[] */
    if(n1 != sizeof(message) ){ *ps= NULL; mfifo_unlock(fifo); return -1;} 
    if(n1 == (size_t) -1){ *ps= NULL; mfifo_unlock(fifo); return -1;} 
    if((*ps)->l == (size_t) 0){ *ps= NULL; mfifo_unlock(fifo); return 0;} 
    
    printf("Taille reçue : %zu\n\n", (*ps)->l);

    printf("******* Lecture du contenu *******\n");
    *ps = realloc( *ps, (*ps)->l + sizeof(message) ); /* agrandir la mémoire pour le tableau */
    n2= mfifo_read(fifo, (*ps)->mes , (*ps)->l ); /* lire le message lui même */
    if(n2 == (size_t) -1){ ps= NULL; mfifo_unlock(fifo); return -1;} 

    mfifo_unlock(fifo);
    return n1+n2;
}

int main(int argc, char** argv){
    mfifo *fifo= NULL;
    mfifo *fifo_created= NULL;

    char *mfifo_name = NULL;
    char *text= NULL;

    char *tmp= NULL;
    char *mfifo_name_tmp= NULL;

    char res[MAX_LEN];
    int nbRead;

    size_t len = 0;
    ssize_t lineSize = 0;
    int c= -2; 
    int fifo_i= -2; 

    unsigned flags= 0;
    mode_t mode= S_IWUSR | S_IRUSR | S_IXUSR;
    size_t capacity= MFIFO_CAPACITY;

    fifos= (mfifo**) malloc( sizeof(mfifo*) );
    srand(time(0)); 

    printf("Bienvenue dans le programme mFifo !\n\n");

    do{
        printf("Veuillez choisir le mode de connexion à la mfifo\n\n");
        printf("1) Connexion à une mFifo existante.\n");
        printf("2) Création d'une mFifo anonyme.\n");
        printf("3) Connexion à une mFifo avec création si non existante.\n");
        printf("4) Connexion et création à une mFifo non existante.\n");
        printf("5) Supprimer une mFifo.\n");
        

        if(c == -1)
            printf("Veuillez choisir un mode existant\n");
        printf("=> ");
        scanf(" %d",&c);
        getchar();

        if(c == 5){
            printf("Veuillez entré le nom de la mFifo a supprimer.\n");
            printf("=> ");

            lineSize = getline(&mfifo_name_tmp, &len, stdin);
            mfifo_name= realloc(mfifo_name, sizeof(char) * lineSize);
            strncpy(mfifo_name, mfifo_name_tmp, lineSize-1);    
            printf("\n");
            mfifo_unlink(mfifo_name);
            return 0;
        }

        switch(c){
            case 1: flags= 0; break;
            case 2: case 3: flags= O_CREAT; break;
            case 4: flags= O_CREAT | O_EXCL; break;
            default: c=-1; break;
        }

    }while(c == -1);
    
    if(c != 2){
        printf("Veuillez entré le nom de votre mFifo.\n");
        printf("=> ");

        lineSize = getline(&mfifo_name_tmp, &len, stdin);
        mfifo_name= realloc(mfifo_name, sizeof(char) * lineSize);
        strncpy(mfifo_name, mfifo_name_tmp, lineSize-1);    
        printf("\n");
        fifo_created= mfifo_connect( mfifo_name, flags, mode, capacity );
    }
    else
        fifo_created= mfifo_connect( NULL, flags, mode, capacity );

    if(fifo_created == NULL)
        return -1;

    fifos[fifos_connected++]= fifo_created;
    //memmove(&fifos[fifos_connected++], &fifo_created, sizeof(mfifo) + fifo_created->capacity );

    printf("Vous êtes désormais connecté à la mFifo /.%s !", fifo_created->name);


    int n;
    message *ps= malloc(sizeof(message));
    pid_t pidChild;
    char *text_parent= "000 ";
    char *text_child= "111 ";
    do{
        mfifo_name_tmp= NULL;
        tmp= NULL;
        fifo= NULL;
        fifo_created= NULL;

        if(fifos_connected != 1){
            do{
                printf("Veuillez choisir une mFifo a manipuler\n");
                for(int i=0; i<fifos_connected; i++){
                    printf("%d) mFifo %s\n", i, fifos[i]->name);
                }
                printf("\n");
                printf("=> ");
                scanf(" %d",&fifo_i);
                getchar();
                printf("\n\n");
            }while(fifo_i < 0 || fifo_i >=fifos_connected);
            fifo= fifos[fifo_i];
        }
        else
            fifo= fifos[0];

        do{
        printf("\n\n ************** Actions possible sur la mFifo ************** \n\n");

        printf("******* Gestion de base *******\n\n");
        printf("1) Lire un donnée dans la mFifo en lock (10x)\n");
        printf("2) Ecrire une donnée dans la mFifo (5s)\n\n");

        printf("3) Lire un message dans la mFifo (5s)\n");
        printf("4) Ecrire un message dans la mFifo (5s)\n");
        
        printf("******* Tests possible (fork) *******\n\n");

        printf("5) Write dans la mFifo en mode write\n");
        printf("6) Write dans la mFifo en mode trywrite\n");
        printf("7) Write dans la mFifo en mode write-partial\n\n");

        printf("8) Write dans la mFifo en mode write/trywrite\n");
        printf("9) Write dans la mFifo en mode write/write-partial\n");
        printf("10) Write dans la mFifo en mode trywrite/write-partial\n\n");


        printf("******* Autres *******\n\n");
        printf("11) Posé le lock en lecture \n");
        printf("12) Levé le lock en ecriture\n");
        printf("13) Gérer une autre mFifo existante\n");
        printf("14) Exec : echo execution !\n\n");
            
        printf("******* Info et parametre *******\n\n");
        printf("15) Etat du mFifo\n");
        printf("16) Deconnexion de la mFifo\n");
        printf("17) Suppression de la mFifo\n\n");

        if(c == -1)
            printf("Veuillez choisir un mode existant\n");

        printf("=> ");
        scanf(" %d",&c);
        getchar();
        printf("\n\n");

        if(c == 2 || c == 4){
            printf("Texte à écrire\n=> ");
            lineSize = getline(&tmp, &len, stdin);
            text= realloc(text, sizeof(char) * lineSize);
            strncpy(text, tmp, lineSize);
            lineSize--;
            printf("\n");
        }
        else
            lineSize= ( pidChild == 0 ? strlen(text_child) : strlen(text_parent) );
        if(c>=3 && c<=10)
            pidChild= fork();
        n= 0;
        
        if(pidChild > 0) for(int j=0; j<20000; j++);    //commencer la boucle a peu pres au meme moment

        switch(c){

            case 1: 
                if( mfifo_lock(fifo) == 0 ){
                    printf("[INFO] Lecture de 10 x 50 octets dans la mFifo.\n");
                    for(int i=0; i<10; i++){
                    printf("\n*** ETAPE %d/10 ***\n", i+1);
                        printf("Lecture de 100 octets dans la mFifo\n");
                        nbRead= mfifo_read(fifo, &res, 100);
                        res[nbRead]= '\0';
                        if(nbRead != 0)
                            printf("%d octet(s) reçu(s) : \n%s\n", nbRead, res);
                        sleep(1);
                    }
                    mfifo_unlock(fifo);
                }
                break;


            case 2:
                for(int i=0; i<10; i++){
                    printf("Ecriture en mode write de %lu octets dans la mFifo.\n", lineSize);
                    n= mfifo_write(fifo, text, lineSize);
                    sleep(1);
                }
                break;


            case 3: 
                printf("[INFO] Lecture de 2 x 70 messages dans la mFifo. (fork) \n\n");
                for(int i=0; i<70; i++){
                    printf("[%s] Lecture du message %d dans la mFifo.\n", 
                        pidChild == 0 ? "ENFANT" : "PARENT", i+1);
                    n= fifo_receiveMessage(fifo, &ps);
                    if(n != 0)
                        printf("[%s] %d octet(s) reçu(s). Message de %zu caractères : %s\n", 
                        pidChild == 0 ? "ENFANT" : "PARENT", n, ps->l, ps->mes);
                }
                break;


            case 4:
                printf("[INFO] Ecriture de 2 x 70 messages dans la mFifo. (fork) \n\n");
                for(int i=0; i<70; i++){
                    printf("[%s] Ecriture du message %d dans la mFifo.\n", 
                        pidChild == 0 ? "ENFANT" : "PARENT", i+1);
                    n= fifo_sendMessage(fifo, text, lineSize);
                    if(n != 0)
                        printf("%d octet(s) reçu(s). Message de %zu caractères : \n%s\n", n, ps->l, ps->mes);
                }
                break;


            case 5: 
                for(int i=0; i<80; i++){
                    printf("\n\n*** ETAPE %d/80 - %s ***\n", i+1, pidChild == 0 ? "ENFANT" : "PARENT" );
                    printf("[INFO] Ecriture en mode write de %lu octets dans la mFifo.\n", lineSize);
                    n= mfifo_write(fifo, pidChild == 0 ? text_child : text_parent, lineSize);
                    if(n == -1)
                        printf("*** ETAPE %d/80 - %s - ERROR ***\n", i+1, pidChild == 0 ? "ENFANT" : "PARENT" );
                }
                break;

                
            case 6:
                for(int i=0; i<100; i++){
                    printf("\n\n*** ETAPE %d/100 - %s ***\n", i+1, pidChild == 0 ? "ENFANT" : "PARENT" );
                    printf("[INFO] Ecriture en mode trywrite de %lu octets dans la mFifo.\n", lineSize);
                    n= mfifo_trywrite(fifo, pidChild == 0 ? text_child : text_parent, lineSize);
                    if(n == -1)
                        printf("*** ETAPE %d/100 - %s - ERROR ***\n", i+1, pidChild == 0 ? "ENFANT" : "PARENT" );
                }
                break;


            case 7:
                for(int i=0; i<100; i++){
                    printf("\n\n*** ETAPE %d/100 - %s ***\n", i+1, pidChild == 0 ? "ENFANT" : "PARENT" );
                    printf("[INFO] Ecriture en mode write partial de %lu octets dans la mFifo.\n", lineSize);
                    n= mfifo_write_partial(fifo, pidChild == 0 ? text_child : text_parent, lineSize);
                    if(n == -1)
                        printf("*** ETAPE %d/100 - %s - ERROR ***\n", i+1, pidChild == 0 ? "ENFANT" : "PARENT" );
                }
                break;

            case 8:
                for(int i=0; i<100; i++){
                    printf("*** ETAPE %d/100 - %s ***\n", i+1, pidChild == 0 ? "ENFANT" : "PARENT" );
                    printf("[INFO] Ecriture en mode %s de %lu octets dans la mFifo.\n", 
                        pidChild == 0 ? "write" : "trywrite", lineSize);
                    if(pidChild == 0)
                        n= mfifo_write(fifo, text_child, lineSize);
                    else
                        n= mfifo_trywrite(fifo, text_parent, lineSize);
                    
                    if(n == -1)
                        printf("*** ETAPE %d/100 - %s - ERROR ***\n", i+1, pidChild == 0 ? "ENFANT" : "PARENT" );
                }
                break;


            case 9:
                for(int i=0; i<100; i++){
                    printf("*** ETAPE %d/100 - %s ***\n", i+1, pidChild == 0 ? "ENFANT" : "PARENT" );
                    printf("[INFO] Ecriture en mode %s de %lu octets dans la mFifo.\n", 
                        pidChild == 0 ? "write" : "write-partial", lineSize);
                    if(pidChild == 0)
                        n= mfifo_write(fifo, text_child, lineSize);
                    else
                        n= mfifo_write_partial(fifo, text_parent, lineSize);

                    if(n == -1)
                        printf("*** ETAPE %d/100 - %s - ERROR ***\n", i+1, pidChild == 0 ? "ENFANT" : "PARENT" );
                }
                break;


            case 10:
                for(int i=0; i<100; i++){
                    printf("*** ETAPE %d/100 - %s ***\n", i+1, pidChild == 0 ? "ENFANT" : "PARENT" );
                    printf("[INFO] Ecriture en mode %s de %lu octets dans la mFifo.\n", 
                        pidChild == 0 ? "trywrite" : "write-partial", lineSize);
                    if(pidChild == 0)
                        n= mfifo_trywrite(fifo, text_child, lineSize);
                    else
                        n= mfifo_write_partial(fifo, text_parent, lineSize);

                    if(n == -1)
                        printf("*** ETAPE %d/100 - %s - ERROR ***\n", i+1, pidChild == 0 ? "ENFANT" : "PARENT" );
                }
                break;

            
            case 11:
                mfifo_trylock(fifo);
                break;


            case 12:
                mfifo_unlock(fifo);
                break;


            case 13: 
                printf("Veuillez entré le nom de la mFifo.\n");
                printf("=> ");

                lineSize = getline(&mfifo_name_tmp, &len, stdin);
                free(mfifo_name);
                mfifo_name= malloc(sizeof(char) * lineSize);
                strncpy(mfifo_name, mfifo_name_tmp, lineSize-1);    
                fifo_created= mfifo_connect( mfifo_name, O_CREAT, mode, capacity );

                if(fifo_created == NULL)
                    break;
                fifos= realloc(fifos, (fifos_connected + 1) * sizeof(mfifo*) );
                fifos[fifos_connected++]= fifo_created;
                //memmove(&fifos[fifos_connected++], &, sizeof(mfifo) + fifo_created->capacity );
                break;


            case 14:
                mfifo_unlock_all();
                execlp("echo", "echo", "execution !", NULL);
                exit(0);


            case 15: 
                printf("nom de la mFIfo : %s\n", fifo->name);
                printf("id de la mFIfo : %d\n", fifo->id);
                printf("Capacité de la mFifo : %lu\n", mfifo_capacity(fifo));
                printf("Mémoire libre de la mFifo : %lu\n", mfifo_free_memory(fifo));
                printf("Mémoire occupé de la mFifo : %lu\n\n", fifo->capacity - mfifo_free_memory(fifo) - 1);
                break;


            case 16:
                deleteMfifo(fifo);
                mfifo_disconnect(fifo);
                break;


            case 17:
                printf("%s ok \n", fifo->name);
                mfifo_unlink(fifo->name);
                deleteMfifo(fifo);
                mfifo_disconnect(fifo);
                break;

                
            default: c=-1;
        }

        if(c>=3 && c<=10){
            if(pidChild == 0)
                exit(0);
            while(wait(NULL) != -1);
        }

        if(n != -1 && c<=16 && c!=-1){
            printf("\n\nAPPUYEZ SUR ENTREE POUR CONTINUER\n\n");
            getchar();
        }
        
        }while( c == -1);
    }while( fifos_connected != 0);
    //int test= testPipeNormee(&fifo, MFIFO_NAME);

    // if(test == -1){
    //     mfifo_unlink(MFIFO_NAME);
    //     return -1;
    // }
    return 0;
}

// int main(void)
// {
//   printf("Please enter a line:\n");
//   char *line = NULL;
//   size_t len = 0;
//   ssize_t lineSize = 0;
//   lineSize = getline(&line, &len, stdin);
//   printf("You entered %s, which has %zu chars.\n", line, lineSize - 1);
// lineSize = getline(&line, &len, stdin);
//   printf("You entered %s, which has %zu chars.\n", line, lineSize - 1);
//   free(line);
//   return 0;
// }