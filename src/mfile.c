#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <semaphore.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>


#include "../inc/mfile.h"

static void _printf(char str[], unsigned mode){
    if(mode == _DEBUG_PRINTF_ && __DEBUG__)
        printf("[DEBUG] %s\n", str);
    else if(mode == _INFO_PRINTF_)
        printf("[INFO] %s\n", str);
    else if(mode == _ERROR_PRINTF_)
        printf("[ERROR %d] %s\n", errno, str);
}
static void debug(char str[]){ _printf(str, _DEBUG_PRINTF_); }
static void info(char str[]){ _printf(str, _INFO_PRINTF_); }
static void error(char str[]){ _printf(str, _ERROR_PRINTF_); }

// static unsigned addBit(unsigned x, unsigned i){
//     return x|= i;
// }
// static unsigned remBit(unsigned x, unsigned i){
//     return x&= i;
// }
// static unsigned hasBit(unsigned x, unsigned i){
//     return !( (x&i) != x);
// }

static int rand_fifo_id()
{
    return (rand() % ((99999) + 1 - 1000)) + 1000;
}

static int fifo_sync(mfifo *fifo){ return msync(fifo, sizeof(mfifo) + fifo->capacity, MS_SYNC); }
// static int fifo_async(mfifo *fifo){ return msync(fifo, sizeof(mfifo) + fifo->capacity, MS_ASYNC); }
static int fifo_invalidate(mfifo *fifo){  return msync(fifo, sizeof(mfifo) + fifo->capacity, MS_INVALIDATE) && fifo_sync(fifo); }

static int fifo_w_wait(mfifo *fifo){  
    fifo_invalidate(fifo);
    int n= sem_wait(fifo->sem_write);
    fifo_invalidate(fifo);
    return n;
}
static int fifo_w_trywait(mfifo *fifo){  
    fifo_invalidate(fifo);
    int n= sem_trywait(fifo->sem_write);
    fifo_invalidate(fifo);
    return n;
}
static int fifo_w_post(mfifo *fifo){  
    fifo_invalidate(fifo);
    int n= sem_post(fifo->sem_write);
    fifo_invalidate(fifo);
    return n;
}

// static int fifo_r_wait(mfifo *fifo){  
//     fifo_invalidate(fifo);
//     int n= sem_wait(fifo->sem_read);
//     fifo_invalidate(fifo);
//     return n;
// }

static int fifo_r_trywait(mfifo *fifo){ 
    fifo_invalidate(fifo); 
    int n= sem_trywait(fifo->sem_read);
    fifo_invalidate(fifo);
    return n;
}
static int fifo_r_post(mfifo *fifo){
    fifo_invalidate(fifo);  
    int n= sem_post(fifo->sem_read);
    fifo_invalidate(fifo);
    return n;
}

static int fifo_pid_w_post(mfifo *fifo){
    fifo_invalidate(fifo);  
    int n= sem_post(fifo->sem_pid_write);
    fifo_invalidate(fifo);
    return n;
}

static int fifo_pid_r_post(mfifo *fifo){
    fifo_invalidate(fifo);  
    int n= sem_post(fifo->sem_pid_read);
    fifo_invalidate(fifo);
    return n;
}

static int fifo_pid_w_wait(mfifo *fifo){  
    fifo_invalidate(fifo);
    int n= sem_wait(fifo->sem_pid_write);
    fifo_invalidate(fifo);
    return n;
}
static int fifo_pid_r_wait(mfifo *fifo){  
    fifo_invalidate(fifo);
    int n= sem_wait(fifo->sem_pid_read);
    fifo_invalidate(fifo);
    return n;
}

static size_t fifo_occupied_len(mfifo *fifo){
    fifo_invalidate(fifo);
    size_t i_debut= fifo->debut;
    size_t i_fin= fifo->fin;
    
    if( i_debut <= i_fin)
        return i_fin - i_debut; 
    
    return fifo->capacity - (i_debut - i_fin);
}


mfifo* mfifo_connect( const char* name_c, int flags, mode_t mode, size_t capacity ){
    mfifo *fifo= NULL;
    sem_t *sem= NULL;     sem_t *sem_r= NULL;
    sem_t *sem_pid= NULL;     sem_t *sem_pid_r= NULL;
    char *sem_name_w= NULL; char *sem_name_r= NULL;
    char *sem_name_pid_w= NULL; char *sem_name_pid_r= NULL;

    char *name= NULL;
    if(name_c != NULL){
        name= (char*) malloc(sizeof(char) * ( strlen(name_c) + 3 ) ); 
        strcpy(name, "/."); strcat(name, name_c);
    }
    
    capacity= capacity < MAX_FIFO_LEN ? capacity :  MAX_FIFO_LEN;
    int fifo_fd, fifo_size= sizeof(mfifo) + capacity;
    // int sem_write_fd; int sem_read_fd;
    struct stat fifo_st;
    
    int isCreatedFifo;
    

    /* Cas Anonyme */
    if(name == NULL){
        debug("Création mFifo anonyme");
        fifo= mmap(NULL, fifo_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANON, -1, 0);
        isCreatedFifo= 1;
    }


    /* Cas connexion */
    else if(flags == 0){
        isCreatedFifo= 0;
        debug("connexion mFifo nommée");
        if( (fifo_fd= shm_open(name, O_RDWR, mode)) == -1 ){
            error("Erreur de connexion à la mFifo.");
            return NULL;
        }

        fstat(fifo_fd, &fifo_st);
        fifo_size= fifo_st.st_size;

        if((fifo= mmap(NULL, fifo_size, PROT_READ | PROT_WRITE, MAP_SHARED, fifo_fd, 0)) == MAP_FAILED){
            error("Erreur allocation memoire mFifo.");
            return NULL;
        }
    }
    
    /* Cas creation 
        B- Nommée
    */
    else{
        if( (fifo_fd= shm_open(name, flags | O_RDWR, mode)) == -1 ){
            error("Erreur de creation/connexion du mFifo.");
            return NULL;
        }
        
        fstat(fifo_fd, &fifo_st);
        isCreatedFifo= fifo_st.st_size <= 0;

        if( !isCreatedFifo ){    // Si je me suis connectée au mFifo
            debug("mFifo existante. Connexion à la mFifo...");
            fifo_size= fifo_st.st_size;
            printf("Taille de la mFifo %s : %d\n", name, fifo_size);
        }
        else{
            debug("Création de la mFifo...");
            if( ftruncate( fifo_fd, fifo_size ) == -1){
                error("Erreur de redimension du mFifo.");
            return NULL;
            }
        }


        if((fifo= mmap(NULL, fifo_size, PROT_READ | PROT_WRITE, MAP_SHARED, fifo_fd, 0)) == MAP_FAILED){
            error("Erreur allocation memoire mFifo.");
            return NULL;
        }
    }

    int id= getpid();
    /* SEMAPHORE */
    // int nb;
    // if( isCreatedFifo ){ 
    //     nb= snprintf(NULL, 0,"%d",getpid());
    //     sem_ID= malloc( sizeof(char) * (nb +1) );
    //     fifo->sem_ID= getpid();
    //     sprintf(sem_ID, "%d", getpid());
    // }
    // else{
    //     nb= snprintf(NULL, 0,"%zu",fifo->sem_ID);
    //     sem_ID= malloc( sizeof(char) * (nb +1) );
    //     sprintf(sem_ID, "%zu", fifo->sem_ID);
    // }
    char *name_base= NULL;
    if(name_c == NULL){
        int nb;
        nb= snprintf(NULL, 0,"%d",id);
        name_base= malloc( sizeof(char) * (nb +1) );
        sprintf(name_base, "%d", id);
    }else{
        name_base= malloc( sizeof(char) * strlen(name_c) );
        sprintf(name_base, "%s", name_c);
    }
    
    sem_name_r= (char*) malloc( sizeof(char) * ( strlen(SEM_R_SUFFIX) + strlen(name_base) + 1 ) ); 
    sem_name_w= (char*) malloc( sizeof(char) * ( strlen(SEM_W_SUFFIX) + strlen(name_base) + 1 ) ); 

    sem_name_pid_r= (char*) malloc( sizeof(char) * ( strlen(SEM_PID_R_SUFFIX) + strlen(name_base) + 1 ) ); 
    sem_name_pid_w= (char*) malloc( sizeof(char) * ( strlen(SEM_PID_W_SUFFIX) + strlen(name_base) + 1 ) ); 

    
    strcpy(sem_name_r, SEM_R_SUFFIX); strcat(sem_name_r, name_base);
    strcpy(sem_name_w, SEM_W_SUFFIX); strcat(sem_name_w, name_base);

    strcpy(sem_name_pid_r, SEM_PID_R_SUFFIX); strcat(sem_name_pid_r, name_base);
    strcpy(sem_name_pid_w, SEM_PID_W_SUFFIX); strcat(sem_name_pid_w, name_base);
    
    if( (sem= sem_open(sem_name_w, flags | O_RDWR, S_IWUSR | S_IRUSR, 1) ) == SEM_FAILED ){
        error("Erreur de creation du semaphore write du mFifo");
        return NULL;
    }
    if( (sem_r= sem_open(sem_name_r, flags | O_RDWR, S_IWUSR | S_IRUSR, 1) ) == SEM_FAILED ){
        error("Erreur de creation du semaphore read du mFifo");
        return NULL;
    }

    if( (sem_pid= sem_open(sem_name_pid_w, flags | O_RDWR, S_IWUSR | S_IRUSR, 1) ) == SEM_FAILED ){
        error("Erreur de creation du semaphore pid write du mFifo");
        return NULL;
    }
    if( (sem_pid_r= sem_open(sem_name_pid_r, flags | O_RDWR, S_IWUSR | S_IRUSR, 1) ) == SEM_FAILED ){
        error("Erreur de creation du semaphore pid read du mFifo");
        return NULL;
    }
    free(sem_name_pid_r); free(sem_name_pid_w);
    free(sem_name_r); free(sem_name_w);

    fifo->sem_write= sem;
    fifo->sem_read= sem_r;

    fifo->sem_pid_write= sem_pid;
    fifo->sem_pid_read= sem_pid_r;

    if( isCreatedFifo ){
        info("Creation de la mFifo terminée.");
        fifo->id= id;
        strcpy(fifo->name, name_base);
        fifo->capacity= capacity;
        fifo->debut= 0;
        fifo->fin= 0;
        fifo->pid_read=  (pid_t) -1;
        fifo->pid_write= (pid_t) -1;
        fifo_invalidate(fifo);
        info("Votre mFifo est initialisée.\n");
    }
    else
        info("Connecté à la mFifo.\n");
    
    return fifo;
}

int mfifo_disconnect(mfifo *fifo){
    debug("Deconnexion à la mFifo en cours...");
    //Si le processus a le verrou sur la fifo

    // Exclusion non necessaire (Aucun processus ne changera la valeur de pid_read si ce processus est alive)
    if(getpid() == fifo->pid_read)  fifo->pid_read=  (pid_t) -1;
    if(getpid() == fifo->pid_read)  fifo->pid_write=  (pid_t) -1;
    fifo_invalidate(fifo);

    if( munmap(fifo, 100) != 0){
        error("error munmap fifo");
        return -1;
    }

    info("Vous êtes deconnecté de la mFifo.");
    return 0;
}

int mfifo_unlink(const char *name_c){
    debug("Supression de la mFifo en cours...\n");
    size_t c= strlen(name_c);
    char *sem_name_r= (char*) malloc(sizeof(char) * ( strlen(SEM_R_SUFFIX) + c + 1 ) ); 
    char *sem_name_w= (char*) malloc(sizeof(char) * ( strlen(SEM_W_SUFFIX) + c + 1 ) ); 

    char *sem_name_pid_r= (char*) malloc(sizeof(char) * ( strlen(SEM_PID_R_SUFFIX) + c + 1 ) ); 
    char *sem_name_pid_w= (char*) malloc(sizeof(char) * ( strlen(SEM_PID_W_SUFFIX) + c + 1 ) ); 

    strcpy(sem_name_r, SEM_R_SUFFIX); strcat(sem_name_r, name_c);
    strcpy(sem_name_w, SEM_W_SUFFIX); strcat(sem_name_w, name_c);
    
    strcpy(sem_name_pid_r, SEM_PID_R_SUFFIX); strcat(sem_name_pid_r, name_c);
    strcpy(sem_name_pid_w, SEM_PID_W_SUFFIX); strcat(sem_name_pid_w, name_c);

    if(sem_unlink(sem_name_w) == -1){ debug("Erreur supression sem write"); }
    if(sem_unlink(sem_name_r) == -1){ debug("Erreur supression sem read"); }
    free(sem_name_w);
    free(sem_name_r);

    if(sem_unlink(sem_name_pid_w) == -1){ debug("Erreur supression sem pid write"); }
    if(sem_unlink(sem_name_pid_r) == -1){ debug("Erreur supression sem pid read"); }
    free(sem_name_pid_r);
    free(sem_name_pid_w);

    char *name= NULL;

    if(name_c != NULL){
        name= malloc(sizeof(char) * ( strlen(name_c) + 3 ) ); 
        strcpy(name, "/."); strcat(name, name_c);
    }

    if(name == NULL){
        info("Supression de la mFifo anonyme effectuée. Vos fils ne pourront plus s'y connecter.\n");
        return 0;
    } 
    
    if(shm_unlink(name) == -1){
        error("Erreur lors de la suppression de la mFifo\n");
        return -1;
    }
    free(name);
    info("Supression de la mFifo effectuée. Plus aucun programme ne pourra se connecter à celle-ci.\n");
    return 0;
}

int mfifo_write(mfifo *fifo, const void *buf, size_t len){
    int capacity= fifo->capacity;

    if(len > capacity){
        error("La donnée est trop grande pour la mFifo.");
        printf("Capacité maximale : %d\n\n", capacity );
        errno= EMSGSIZE;
        return -1;
    }

    if(mfifo_free_memory(fifo) < len)
        info("Espace libre dans la mFifo insuffisant");
    
    info("Attente d'écriture dans la mFifo...");
    do{
        while(mfifo_free_memory(fifo) < len);

        fifo_pid_w_wait(fifo);
        if( fifo->pid_write !=  (pid_t) -1 && getpgid( fifo->pid_write ) == (pid_t) -1 && errno == ESRCH ){        // le verrou est posé sur un processus INEXISTANT
            fifo->pid_write= (pid_t) -1; fifo_invalidate(fifo);
            printf("[INFO] Le verrou en ecriture du processus %d est levé.\n", fifo->pid_write);
            fifo_w_post(fifo);
            fifo_invalidate(fifo);
        }
        fifo_pid_w_post(fifo);
    }while(fifo_w_wait(fifo));
    
    fifo_pid_w_wait(fifo);
    fifo->pid_write= getpid();
    fifo_pid_w_post(fifo);

    fifo_invalidate(fifo);

    debug("Ecriture dans la mFifo...");

    for(int i=0; i<len; i++)
        memmove(&fifo->memory[fifo->fin++ % capacity], buf++, 1);
    fifo->fin%= capacity;
    info("Ecriture dans la mFifo terminée.\n");

    fifo_pid_w_wait(fifo);
    fifo->pid_write= (pid_t) -1;
    fifo_pid_w_post(fifo);

    fifo_w_post(fifo);

    return 0;
}
int mfifo_trywrite(mfifo *fifo, const void *buf, size_t len){
    int capacity= fifo->capacity;
    int free_mem= mfifo_free_memory(fifo);
    if(len > capacity){
        errno= EMSGSIZE;
        error("La donnée est trop grande pour la mFifo.");
        printf("==> Capacité maximale : %d\n\n", capacity );
        return -1;
    }
    if(free_mem < (size_t) len){
        error("Espace libre insuffisant.");
        printf("==> Espace libre de la mFifo: %d\n\n", free_mem );
        return -1;
    } 

    fifo_pid_w_wait(fifo);
    if( fifo->pid_write != (pid_t) -1 && getpgid( fifo->pid_write ) == (pid_t) -1 && errno == ESRCH ){        // le verrou est posé sur un processus INEXISTANT
        printf("[INFO] Le verrou en ecriture du processus %d est levé.\n", fifo->pid_write );
        fifo_w_post(fifo);
        fifo->pid_write= (pid_t) -1;
    }
    fifo_pid_w_post(fifo);

    if( fifo_w_trywait(fifo) == -1){
        errno= EAGAIN;
        error("Un processus est déjà en cours d'écriture.");
        return -1;
    }

    fifo_pid_w_wait(fifo);
    fifo->pid_write= getpid();
    fifo_pid_w_post(fifo);

    fifo_invalidate(fifo);
    
    debug("Ecriture dans la mFifo...");
    for(int i=0; i<len; i++)
        memmove(&fifo->memory[fifo->fin++ % capacity], buf++, 1);
    fifo->fin%= capacity;
    info("Ecriture dans la mFifo terminée.\n");

    fifo_pid_w_wait(fifo);
    fifo->pid_write= (pid_t) -1;
    fifo_pid_w_post(fifo);

    fifo_w_post(fifo);
    fifo_invalidate(fifo);

    return 0;
}

// int mfifo_trywrite(mfifo *fifo, const void *buf, size_t len){
//     int capacity= fifo->capacity;
//     if(len > capacity){
//         errno= EMSGSIZE;
//         error("La donnée est trop grande pour la mFifo.");
//         printf("Capacité maximale : %d\n\n", capacity );
//         return -1;
//     }
//     if( fifo_trywait(fifo) == -1){
//         errno= EAGAIN;
//         error("Un processus est déjà en cours d'écriture.");
//         return -1;
//     }
//     if(mfifo_free_memory(fifo) < len)
//         return -1;

//     debug("Ecriture dans la mFifo...");

//     for(int i=0; i<len; i++)
//         memmove(&fifo->memory[fifo->fin++ % capacity], buf++, 1);
        
//     fifo_invalidate(fifo);
//     info("Ecriture dans la mFifo terminée.\n");

//     fifo_post(fifo);
//     return 0;
// }

int mfifo_write_partial(mfifo *fifo, const void *buf, size_t len){
    int nbWrite= 0; size_t lenInit= len;

    info("Ecriture PARTIELLE dans la mFifo en cours.\n");
    do{
        nbWrite= (len < WRITE_PACKET_SIZE ? len : WRITE_PACKET_SIZE);
        fifo_invalidate(fifo);
        mfifo_write(fifo, &buf[lenInit - len], WRITE_PACKET_SIZE); 
        fifo_invalidate(fifo);
        len-= nbWrite;
    }while(len !=0);
    info("Ecriture PARTIELLE dans la mFifo terminée.");
    return 0;
}



ssize_t mfifo_read(mfifo *fifo, void *buf, size_t len){
    int capacity= fifo->capacity;
    int isMyLock= (getpid() == fifo->pid_read);                // Verrou posé ?  
    int fifo_in_len= fifo_occupied_len(fifo);             // element dans le fifo
    int i=0;
    
    len= (len < fifo_in_len ? len : fifo_in_len);
    
    if(fifo_in_len == 0){
        info("Aucune donnée dans la mFifo\n");
        return 0;
    }
        
    info("Lecture dans la mFifo...");

    if( fifo_in_len < len && isMyLock)    // si j'ai déjà posé un verrou (grace a mfifo_lock)
        return -1;
    
    if(!isMyLock && fifo->pid_read != -1){ 
        printf("[INFO] Veuillez patienter le processus %d est en cours de lecture..\n", fifo->pid_read );
        mfifo_lock(fifo);
    }

    for(i=0; i < len && fifo->debut != fifo->fin; i++)
        memcpy(&buf[i], &fifo->memory[fifo->debut++ % capacity], 1);
    fifo->debut%= capacity;

    if(!isMyLock && fifo->pid_read != -1) mfifo_unlock(fifo);

    info("Lecture dans la mFifo terminée.\n");
    return i;
}


int mfifo_lock(mfifo *fifo){
    debug("Verouillage en lecture de la mFifo en cours...");

    if(fifo->pid_read == getpid()){
        printf("Vous avez deja un verrou en lecture.");
        return 0;
    }

    fifo_pid_r_wait(fifo);
    if(fifo->pid_read != -1)
        printf("Le processus %d est déjà en lecture de la mFifo. Patientez...\n", fifo->pid_read);
    fifo_pid_r_post(fifo);

    while( fifo_r_trywait(fifo) == -1){
        if(errno == EAGAIN){

            fifo_pid_r_wait(fifo);
            printf("in\n");
            if( fifo->pid_read != (pid_t) -1 && getpgid( fifo->pid_read ) == (pid_t) -1 && errno == ESRCH ){        // le verrou est posé sur un processus INEXISTANT
                printf("[INFO] L'ancien verrou en lecture du processus %d est levé.\n", fifo->pid_read);
                fifo_r_post(fifo);
                fifo->pid_read= (pid_t) -1; fifo_invalidate(fifo);
            }
            fifo_pid_r_post(fifo);
        }
        else{
            error("Erreur lors du lock en lecture de la mFifo.");
            return -1;
        }
    }

    fifo_pid_r_wait(fifo);
    fifo->pid_read= getpid();
    fifo_pid_r_post(fifo);

    info("Verouillage effectué\n");
    return 0;
}

int mfifo_unlock(mfifo *fifo){
    if(fifo->pid_read != getpid()){
        debug("Vous n'avez pas de lock en lecture");
        return 0;
    }
    if( fifo_r_post(fifo) == -1){
        error("Erreur lors du unlock en lecture de la mFifo");
        return -1;
    }

    fifo_pid_r_wait(fifo);
    fifo->pid_read= (pid_t) -1;
    fifo_pid_r_post(fifo);
    info("Lock levé.");
    return 0;
}

int mfifo_unlock_write(mfifo *fifo){
    if(fifo->pid_write != getpid()){
        debug("Vous n'avez pas de lock en ecriture");
        return 0;
    }
    if( fifo_w_post(fifo) == -1){
        error("Erreur lors du unlock en ecriture de la mFifo");
        return -1;
    }
    fifo_pid_w_wait(fifo);
    fifo->pid_write= (pid_t) -1;
    fifo_pid_w_post(fifo);

    return 0;
}

int mfifo_trylock(mfifo *fifo){
    if(fifo->pid_read == getpid()){
        printf("Vous avez deja un verrou en lecture.");
        return 0;
    }

    if( fifo->pid_read != (pid_t) -1 && getpgid( fifo->pid_read ) == (pid_t) -1 && errno == ESRCH ){        // le verrou est posé sur un processus INEXISTANT
        printf("[INFO] Le verrou en lecture du processus %d est levé.\n", fifo->pid_read);
        fifo_r_post(fifo);

        fifo_pid_r_wait(fifo);
        fifo->pid_read= (pid_t) -1;
        fifo_pid_r_post(fifo);
    }

    if( fifo_r_trywait(fifo) == -1 && fifo->pid_read != -1 ){
        //errno= EAGAIN;
        printf("pid : %d\n",fifo->pid_read);
        error("Un processus est déjà en cours de lecture.");
        return -1;
    }

    fifo_pid_r_wait(fifo);
    fifo->pid_read= getpid();
    fifo_pid_r_post(fifo);
    return 0;
}

size_t mfifo_capacity(mfifo *fifo){ return fifo->capacity;  }
size_t mfifo_free_memory(mfifo *fifo){ return fifo->capacity - fifo_occupied_len(fifo) - 1;    }


// static void _onExit(){ printf("test\n"); mfifo_unlink(MFIFO_NAME); }