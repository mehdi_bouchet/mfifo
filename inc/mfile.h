#ifndef MFILE_H
#define MFILE_H

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <semaphore.h>

#define MAX_FIFO_LEN 4000       // max = 4k
#define __DEBUG__ 1

#define _DEBUG_PRINTF_ 1 << 0
#define  _INFO_PRINTF_ 1 << 1
#define _ERROR_PRINTF_ 1 << 2

#define SEM_SET_R 1 << 0
#define SEM_SET_W 1 << 1
#define SEM_SET_PID_R 1 << 2
#define SEM_SET_PID_W 1 << 3

#define R 0
#define W 0

#define MAX_LEN 64
#define WRITE_PACKET_SIZE 2
    
#define SEM_R_SUFFIX "/.sem_read_"
#define SEM_W_SUFFIX "/.sem_write_"

#define SEM_PID_W_SUFFIX "/.sem_pid_write_"
#define SEM_PID_R_SUFFIX "/.sem_pid_read_"

#define MFIFO_NAME "/.mfifo_ano"
#define MFIFO_CAPACITY 1000


typedef struct{ 
    size_t l;
    char mes[];
} message;

typedef struct{
    int id;
    char name[20];
    size_t capacity;
    size_t debut;
    size_t fin;

    pid_t pid_write;
    pid_t pid_read;

    size_t sem_ID;
    sem_t *sem_write;
    sem_t *sem_read;
    sem_t *sem_pid_write;
    sem_t *sem_pid_read;

    char memory[];
    
} mfifo;

// static void _onExit();

// static void _printf(char str[], unsigned mode);
// static void debug(char str[]);
// static void info(char str[]);
// static void error(char str[]);

// static unsigned addBit(unsigned x, unsigned i);
// static unsigned remBit(unsigned x, unsigned i);
// static unsigned hasBit(unsigned x, unsigned i);


// static int fifo_sync(mfifo *fifo);
// static int fifo_async(mfifo *fifo);
// static int fifo_invalidate(mfifo *fifo);

// static int fifo_wait(mfifo *fifo);
// static int fifo_trywait(mfifo *fifo);
// static int fifo_post(mfifo *fifo);

// static int fifo_r_wait(mfifo *fifo);
// static int fifo_r_post(mfifo *fifo);

// static int fifo_occupied_len(mfifo *fifo);

// static int fifo_sendMessage(mfifo *fifo, void *data, size_t len);


mfifo* mfifo_connect( const char* name_c, int flags, mode_t mode, size_t capacity );
int mfifo_disconnect(mfifo *fifo);
int mfifo_unlink(const char *name_c);

int mfifo_write(mfifo *fifo, const void *buf, size_t len);
int mfifo_trywrite(mfifo *fifo, const void *buf, size_t len);
int mfifo_write_partial(mfifo *fifo, const void *buf, size_t len);

ssize_t mfifo_read(mfifo *fifo, void *buf, size_t len);

int mfifo_lock(mfifo *fifo);
int mfifo_unlock(mfifo *fifo);
int mfifo_unlock_write(mfifo *fifo);
int mfifo_trylock(mfifo *fifo);

size_t mfifo_capacity(mfifo *fifo);
size_t mfifo_free_memory(mfifo *fifo);


#endif